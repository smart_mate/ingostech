# README #

This is a solution of Cleverbots team for IngosTech test task

### How to run? ###

Run **solution.py** in python 3.5.

Or build Docker container for **Dockerfile** and run it.

Enter input string in console and press ENTER.
Press Ctrl+C to exit.

See you at INGOS TECH 7.0!

Your faithfully,

Cleverbots team

www.cleverbots.ru
