class TreeInputError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class TreeStruct:
    TREE_LEAF='leaf'
    TREE_OPERATOR='operator'

    def __init__(self, leaf=None, operator=None, left=None, right=None):
        self.leaf = leaf
        self.left = None
        self.right = None
        self.operator = None

        if leaf is not None:
            self.leaf = leaf
            self.type=TreeStruct.TREE_LEAF
        elif operator is not None and left is not None:
            self.left = left
            self.right = right
            self.operator = operator
            self.type = TreeStruct.TREE_OPERATOR
        else:
            raise TreeInputError(self)

    def __repr__(self):
        s = '<Tree: '
        used_flag=False

        if self.leaf:
            used_flag = True
            s += 'Leaf={} '.format(self.leaf)

        if self.operator:
            used_flag = True
            s += 'Operator={} '.format(self.operator)

        if self.left:
            used_flag = True
            s += 'Left={} '.format(repr(self.left))

        if self.operator:
            used_flag = True
            s += 'Right={} '.format(repr(self.right))

        s += '>'
        if used_flag:
            return s
        else:
            return None
