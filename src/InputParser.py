from .TreeStruct import TreeStruct
import re


class IncorrectInputError(Exception):
    pass

class InputParser:
    CHAR = 'char'
    OPERATOR = 'operator'
    OPEN_PARANT = '('
    CLOSE_PARANT = ')'
    NONE = None

    @staticmethod
    def check_input(input_str):
        last = InputParser.NONE
        opened_parant_count=0
        if input_str is None:
            return

        if input_str == '':
            return

        for char in input_str:
            if char == '(':
                if last in [InputParser.CHAR, InputParser.CLOSE_PARANT]:
                    raise IncorrectInputError
                last = InputParser.OPEN_PARANT
                opened_parant_count += 1
            elif char == ')':
                last = InputParser.CLOSE_PARANT
                opened_parant_count -= 1
                if opened_parant_count < 0:
                    raise IncorrectInputError
            elif char == ' ':
                continue
            elif char in ['+', '-', '*', '/']:
                if last not in [InputParser.CLOSE_PARANT, InputParser.CHAR]:
                    raise IncorrectInputError
                last = InputParser.OPERATOR
            elif char in 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz':
                if last not in [InputParser.NONE, InputParser.OPERATOR, InputParser.OPEN_PARANT, InputParser.CHAR]:
                    raise IncorrectInputError
                last = InputParser.CHAR
            else:
                raise IncorrectInputError

        if opened_parant_count != 0:
            raise IncorrectInputError

        if last in [InputParser.OPEN_PARANT, InputParser.OPERATOR]:
            raise IncorrectInputError

    @staticmethod
    def get_char_type(char):
        if char =='(':
            return InputParser.OPEN_PARANT
        elif char == ')':
            return InputParser.CLOSE_PARANT
        elif char in ['+', '-', '*', '/']:
            return InputParser.OPERATOR
        elif char in 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz':
            return InputParser.CHAR

    # if string contains chars only
    @staticmethod
    def chars_only(strg, search=re.compile(r'[^a-zA-Z]').search):
        return not bool(search(strg))

    @staticmethod
    def tokenize_string(string):
        return re.findall(r"([a-zA-Z]+|\(|\)|\+|\-|\*|\/)", string)


    @staticmethod
    def parse_input(input_str):
        def dict_to_tree(tree_dict):
            if len(tree_dict)==0:
                return None
            elif len(tree_dict)==1:
                return tree_dict[0]
            else:
                for elem_index in range(len(tree_dict)):
                    if tree_dict[elem_index] in ['+', '-']:
                        return TreeStruct(operator=tree_dict[elem_index], left=dict_to_tree(tree_dict[:elem_index]),
                                          right=dict_to_tree(tree_dict[elem_index+1:]))
                for elem_index in range(len(tree_dict)):
                    if tree_dict[elem_index] in ['*', '/']:
                        return TreeStruct(operator=tree_dict[elem_index], left=dict_to_tree(tree_dict[:elem_index]),
                                          right=dict_to_tree(tree_dict[elem_index + 1:]))

        if input_str is None:
            raise IncorrectInputError

        InputParser.check_input(input_str)

        # delete spaces
        input_str = ''.join(list(filter(lambda x: x != ' ', input_str)))

        if input_str == '':
            return TreeStruct(leaf='')

        if InputParser.chars_only(input_str):
            return TreeStruct(leaf=input_str)

        dict_input = InputParser.tokenize_string(input_str)
        dict_temp = []
        i=0
        while i < len(dict_input):
            if dict_input[i] == '(':
                parent_open=1
                j = i+1
                found_close_parent=False
                while j < len(dict_input):
                    if dict_input[j] == '(':
                        parent_open += 1
                    elif dict_input[j] == ')':
                        parent_open -= 1

                    if parent_open == 0:
                        found_close_parent = True
                        dict_temp.append(TreeStruct(operator='()', left=InputParser.parse_input(''.join(dict_input[i+1:j]))))
                        i = j
                        break
                    j += 1
                if not found_close_parent:
                    raise IncorrectInputError
            elif InputParser.chars_only(dict_input[i]):
                dict_temp.append(TreeStruct(leaf=dict_input[i]))
            else:
                dict_temp.append(dict_input[i])

            i += 1


        return dict_to_tree(dict_temp)






