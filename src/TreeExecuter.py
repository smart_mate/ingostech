from .TreeStruct import TreeStruct

def execute(tree):
    #print(repr(tree))
    if tree is None:
        return None

    if tree.type == TreeStruct.TREE_LEAF:
        if tree.leaf:
            return tree.leaf
        else:
            return ''

    if tree.operator:
        if tree.operator == '()':
            return execute(tree.left)
        elif tree.operator == '+':
            return execute(tree.left) + execute(tree.right)
        elif tree.operator == '-':
            left = execute(tree.left)
            right = execute(tree.right)

            if left.endswith(right):
                return left[:len(left)-len(right)]
            else:
                return left
        elif tree.operator == '*':
            left = execute(tree.left)
            right = execute(tree.right)
            res = ''
            min_length = min(len(left), len(right))
            for i in range(min_length):
                res += left[i]
                res += right[i]

            res += left[min_length:]
            res += right[min_length:]
            return res

        elif tree.operator == '/':
            left = execute(tree.left)
            right = execute(tree.right)
            res = ''

            if left[1::2].startswith(right):
                res += left[0:2*len(right):2]
                res += left[2*len(right):]
            else:
                res = left
            return res
