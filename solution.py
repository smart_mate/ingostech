from src.InputParser import InputParser, IncorrectInputError
from src.TreeExecuter import execute
import sys, signal


def sigIntHandler(a, b):
    print('\nBye-bye! See you at INGOS TECH 7.0!')
    exit()

signal.signal(signal.SIGINT, sigIntHandler)

while True:
    print ('Enter input string, press Ctrl+C to exit')
    str = input()
    try:
        print(execute(InputParser.parse_input(str)))
        #print('True')
    except IncorrectInputError:
         print('Incorrect input')
