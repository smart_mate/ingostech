import unittest
from src.TreeExecuter import execute
from src.InputParser import InputParser, IncorrectInputError

def make_positive_test(query, answer):
    def _test(self):
        self.assertEqual(execute(InputParser.parse_input(query)), answer)
    return _test

def make_negative_test(query):
    def _test(self):
        with self.assertRaises(IncorrectInputError):
            execute(InputParser.parse_input(query))
    return _test


statements_positive = [['abc + xyz', 'abcxyz'],
                       ['xyz + abc', 'xyzabc'],
                       ['abcxyz - xyz', 'abc'],
                       ['abc - xyz', 'abc'],
                       ['abcxyzd - xyz', 'abcxyzd'],
                       ['abc * xyz', 'axbycz'],
                       ['abc * xyzps', 'axbyczps'],
                       ['abcps * xyz', 'axbyczps'],
                       ['abc / xyz', 'abc'],
                       ['axbycz / xyz', 'abc'],
                       ['axbyczps / xyz', 'abcps'],
                       ['((index-ex)-d)+gst*osr+(an+k+oh)/(n+o)', 'ingosstrakh'],
                       ['()','']
                       ]

statements_negative = [')(', '(s))', '((sa)', 'a++a', 'a(v)']

class TestProgramm(unittest.TestCase):
    pass

if __name__ == '__main__':

    for statement in statements_positive:
        query, answer = statement[0], statement[1]
        test = make_positive_test(query, answer)
        test.__name__ = "test_positive{}".format(statements_positive.index(statement))
        setattr(TestProgramm, test.__name__, test)

    for statement in statements_negative:
        query = statement
        test = make_negative_test(query)
        test.__name__ = "test_negative{}".format(statements_negative.index(statement))
        setattr(TestProgramm, test.__name__, test)

    unittest.main()